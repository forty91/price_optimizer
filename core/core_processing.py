import logging
import pandas
import data_format
import time
import optimizer_status as opst
import numpy as np
import core_config as cfg

from errors import ErrorOptimizer, ErrorInvalidPriceModel
from factory_optimizer import create_optimizer

logger = logging.getLogger('core_processing')


def fix_max_min_prices(price_model, task_size, min_prices, max_prices):
    """ 
    Fixes the potential problem with negative demand. When the price is too 
    high, the demand could be negative due to linear price model kx + b. 
    Therefore, we need to restrict the max_price from above. 
    """
    
    k_tol = 0.0001
    min_max_tol = 0.001
    
    msg = str()
    changed_max_p = False
    
    for i in range(task_size):
        ki = abs(price_model.k[i])
        bi = price_model.b[i]
        
        if ki < k_tol:
            continue
        
        max_price = ((1. * bi) / ki) - min_max_tol
        
        if max_price >= max_prices[i]:
            continue
        
        changed_max_p = True
        
        max_prices[i] = max_price
        if max_price < min_prices[i]:
            raise ErrorInvalidPriceModel('Invalid model '
                                         '{} {} {}'.format(i, 
                                                           max_price, 
                                                           min_prices[i]))
        
    if changed_max_p:
        msg = 'decreased max price'
        
    return msg


def preprocess_data(task_size, min_prices, max_prices, price_model):
    """ 
    Checks whether the task is physically correct. Updates input data to make 
    task physically correct.
    """
    
    message = str()
    
    k_is_above_0 = True
    
    for i in range(len(price_model.k)):
        if price_model.k[i] > 0.:
            price_model.k[i] = 0.
            k_is_above_0 = False
                
    for i in range(len(price_model.b)):
        if price_model.b[i] < 0.:
            price_model.b[i] = 0.
    
    if not k_is_above_0:
        msg = "fixed positive k"
        message = extend_message(message, msg)
        
    msg = fix_max_min_prices(price_model, task_size, min_prices, max_prices)
    message = extend_message(message, msg)
    
    return message


def extend_message(msg1, msg2):
    if msg2:
        if msg1:
            msg1 += ';'
        
        msg1 += msg2
    
    return msg1


def optimize_and_prepare_result(data, demand_constr, task_str, optimizer_str):
    """ 
    The function performs optimization and saves results into the file.
    """
    
    opt_prices = np.nan
    opt_demand = float('nan')
    opt_profit = float('nan')
    
    orig_demand = float('nan')
    orig_profit = float('nan')
    
    message = str()
    
    try:
        task_size = data.shape[0]
        logger.info('Task size: {}'.format(task_size))
        
        unpacked_data = data_format.unpack_data(data)
        
        price_model = unpacked_data[3]
        msg = preprocess_data(task_size, *unpacked_data[1:])
        message = extend_message(message, msg)
        
        optimizer = create_optimizer(unpacked_data, 
                                     task_size, 
                                     demand_constr, 
                                     task_str,
                                     optimizer_str)
        
        start_time = time.time()
        logger.info('Optimizing...')
        opt_prices, result, msg = optimizer.optimize()
        logger.info('Done. Elapsed time: {}'.format(time.time() - start_time))
        if cfg.debug_mode:
            message = extend_message(message, msg)

            orig_prices = unpacked_data[0]

            orig_demand = price_model.total_demand(orig_prices)
            orig_profit = price_model.total_profit(orig_prices)

            opt_profit = price_model.total_profit(opt_prices)
            opt_demand = price_model.total_demand(opt_prices)
        
    except (ErrorOptimizer, 
            data_format.ErrorInputDataFormat, 
            ErrorInvalidPriceModel) as e:
        result = opst.NOT_OPTIMISED
        msg = str(e)
        message = extend_message(message, msg)
    
    data_format.save_results(data, 
                             orig_profit, 
                             orig_demand, 
                             opt_prices, 
                             opt_demand, 
                             opt_profit, 
                             result,
                             message)


def load_optimize_and_save(input_file_path, 
                           output_file_path, 
                           demand_constr,
                           task_str, 
                           optimizer_str):
    """ 
    The function loads data, performs optimization and saves results to file. 
    """
    
    logger.info('Loading input data...')

    data = pandas.read_csv(input_file_path, 
                           sep = ';', 
                           encoding = 'cp1251', 
                           decimal = ',')
    
    logger.info('Done.')
    
    optimize_and_prepare_result(data, demand_constr, task_str, optimizer_str)
    
    logger.info('Saving results...')
    
    class CommFloat:
        def __mod__(self, x):
            return str(x).replace('.',',')

    if type(input_file_path) is not str and type(input_file_path) is not unicode:
        input_file_path.close() # For reading and writing into the same file    
        
    data.to_csv(output_file_path, 
                sep = ';', 
                encoding = 'cp1251', 
                float_format = CommFloat(), 
                index = False)
    
    logger.info('Done.')

