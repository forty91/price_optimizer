"""
Provides a class that finds prices for maximum profit with constraints using 
gradient-based solver.
"""

from core.profit_optimizer_base import ProfitOptimizerBase
import core.optimizer_status as opst
import scipy.optimize
import math
import core.core_config as cfg


def tune_tolerance(task_size):
    """ 
    Tunes tolerance for different task sizes. It allows more precision for 
    simple tasks. 
    """
    
    return max(1e-5, 
               min(0.1, 
                   0.1 ** math.floor(5000. / 
                                     ((task_size + 50) * 
                                      math.sqrt(1. * task_size / 100)))))
    

class ProfitOptimizerGrad(ProfitOptimizerBase):
    """ 
    Performs optimization of profit via gradient-based method.
    Slow on big tasks with number of goods > 1500.
    """
    
    def __init__(self, *args, **kwargs):
        """
        Parameters for initializations:
            orig_prices,
            min_prices, 
            max_prices,
            price_model, 
            task_size, 
            demand_constr
        """
        
        super(ProfitOptimizerGrad, self).__init__(*args, **kwargs)
    
    def optimize(self):
        """ Starts optimization procedure. """
        
        res = scipy.optimize.minimize(fun = self._opt_func, 
                                      x0 = self.orig_prices, 
                                      jac = self._fprime_finc,
                                      method = 'SLSQP', 
                                      bounds = zip(self.min_prices, 
                                                   self.max_prices),
                                      constraints = {'type' : 'eq', 
                                                     'fun' : self._eq_cons, 
                                                     'jac' : self._eq_jac},
                                      options = {'disp' : cfg.debug_mode},
                                      tol = tune_tolerance(self.task_size))
        
        if res.success:
            if self.check_constraints(res.x):
                result = opst.OPTIMAL
            else:
                result = opst.INCONSIST_CONSTR
        else:
            result = opst.INCONSIST_CONSTR
            
        return res.x, result, res.message
    
    def _eq_cons(self, prices):
        return self.price_model.total_demand(prices) - self.demand_constr
    
    def _opt_func(self, prices):
        return -1. * self.price_model.total_profit(prices)
    
    def _part_prime(self, price, k, b, purch_price):
        return 2. * k * price + b - purch_price * k
    
    def _fprime_finc(self, prices):
        return -1. * self._part_prime(prices, 
                                     k = self.price_model.k, 
                                     b = self.price_model.b, 
                                     purch_price = self.price_model.purch_price)
    
    def _eq_jac(self, prices):
        return self.price_model.k

