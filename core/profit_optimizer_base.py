import utils 


class ProfitOptimizerBase(object):
    """ Base class for profit maximization with fixed demand. """
    
    def __init__(self,
                 orig_prices,
                 min_prices, 
                 max_prices,
                 price_model, 
                 task_size, 
                 demand_constr):

        self.task_size = task_size
        
        self.price_model = price_model
        
        self.min_prices = min_prices
        self.max_prices = max_prices
        self.orig_prices = orig_prices
        self.demand_constr = demand_constr
    
    def check_constraints(self, prices):
        """ 
        Checks whether constraints are satisfied with current variables. 
        """
        
        tol_min_max_price = 0.1
        tol_eq_constr = 0.001
        abs_tol = 0.01
        
        check = (prices - self.max_prices > tol_min_max_price).sum()
        if check > 0:
            return False
        
        check = (prices - self.min_prices < -tol_min_max_price).sum()
        if check > 0:
            return False
        
        if not utils.check_tol_eq(self.price_model.total_demand(prices), 
                                  self.demand_constr,
                                  tol_eq_constr,
                                  abs_tol):
            return False
        
        return True
