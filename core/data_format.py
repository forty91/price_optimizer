import numpy
from core import price_model
from core.errors import ErrorInputDataFormat

COL_PURCH_PRICE = 'PURCH_PRICE'
COL_ORIG_PRICE = 'ORIG_PRICE'
COL_MIN_PRICE = 'MIN_PRICE'
COL_MAX_PRICE = 'MAX_PRICE'
COL_PRICE_MODEL_K = 'K'
COL_PRICE_MODEL_B = 'B'

COL_RESULT = 'PROCESS_RESULT'
COL_MESSAGE = 'MESSAGE'

COL_ORIG_PROFIT = 'ORIG_PROFIT'
COL_ORIG_DEMAND = 'ORIG_DEMAND'
COL_OPT_PROFIT = 'OPT_PROFIT'
COL_OPT_DEMAND = 'OPT_DEMAND'
COL_OPT_PRICE = 'OPT_PRICE'

RESULT_SUCCESS = 'OPTIMAL'
RESULT_UNKNOWN = 'UNKNOWN'


def unpack_data(data):
    """ Extracts input data from db. """
    
    try:
        # return (data[COL_ORIG_PRICE].values,
        #         data[COL_MIN_PRICE].values,
        #         data[COL_MAX_PRICE].values,
        #         price_model.PriceModel(data[COL_PRICE_MODEL_K].values,
        #                                data[COL_PRICE_MODEL_B].values,
        #                                data[COL_PURCH_PRICE].values))

        unpacked_data = (
            numpy.asarray([r.orig_price for r in data]),
            numpy.asarray([r.min_price for r in data]),
            numpy.asarray([r.max_price for r in data]),

            price_model.PriceModel(numpy.asarray([r.k for r in data]),
                                   numpy.asarray([r.b for r in data]),
                                   numpy.asarray([r.purch_price for r in data]))
        )

        return unpacked_data

    except KeyError as e:
        raise ErrorInputDataFormat("Failed to find column: {}.".format(str(e)))
    

def save_results(data,
                 orig_profit,
                 orig_demand,
                 opt_prices,
                 opt_demand,
                 opt_profit,
                 result,
                 message):
    """ Saves resutls back to csv. """
    
    data.loc[:, COL_OPT_PRICE] = opt_prices
    data.ix[0, COL_ORIG_DEMAND] = orig_demand
    data.ix[0, COL_ORIG_PROFIT] = orig_profit
    data.ix[0, COL_OPT_DEMAND] = opt_demand
    data.ix[0, COL_OPT_PROFIT] = opt_profit
    data.ix[0, COL_RESULT] = result
    data.ix[0, COL_MESSAGE] = message
    