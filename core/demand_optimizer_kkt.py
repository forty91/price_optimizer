"""
Provides a class that finds prices for maximum demand with constraints using 
kkt-based solver.
"""

from core.demand_optimizer_base import DemandOptimizerBase
import numpy as np
import picos as pic
import cvxopt
import core.optimizer_status as opst
import math
import core.core_config as cfg

import logging
logger = logging.getLogger('core_processing')


class Scaler(object):
    """ 
    Scaler makes the task computationally feasible (enhances numerical 
    stability). 
    """
    
    def __init__(self, price_model, max_prices):
        super(Scaler, self).__init__()
        self.price_model = price_model
        self.max_prices = max_prices
        
    @property
    def target(self):
        return 1. # Looks ok
    
    @property
    def mm(self):
        #return 0.01 # DEBUG:
        return min(1., 0.1 ** int(self.max_prices.sum() / 30e+4))
    
    @property
    def prof(self):
        #return 0.0001 # DEBUG:
        return 0.1 ** round(math.log(abs(self.price_model.k).sum(), 10)) 
    

class DemandOptimizerKkt(DemandOptimizerBase):
    """ 
    Maximizes demand by solving second order cone program via 
    Karush-Kuhn-Tucker conditions 
    """
    
    def __init__(self, *args, **kwargs):
        """
        Parameters for initializations:
            orig_prices,
            min_prices, 
            max_prices,
            price_model, 
            task_size, 
            profit_constr
        """
        
        super(DemandOptimizerKkt, self).__init__(*args, **kwargs)
                    
    def optimize(self):
        """ Starts optimization procedure. """
        
        scaler = Scaler(self.price_model, self.max_prices)
        
        msg = str()
        
        prob = pic.Problem()
        
        p = prob.add_variable('p', self.task_size)
        
        g = scaler.target * self.price_model.k.reshape(-1, 1)
        
        objective = (p.T * cvxopt.matrix(g))
        prob.set_objective('max', objective)
        
        ########################################################################
        
        min_price_constr = (p * scaler.mm >= 
                            cvxopt.matrix(scaler.mm * 
                                          self.min_prices.reshape(-1, 1)))
        prob.add_constraint(min_price_constr)
        
        max_price_constr = (p * scaler.mm <= 
                            cvxopt.matrix(scaler.mm * 
                                          self.max_prices.reshape(-1, 1)))
        prob.add_constraint(max_price_constr)
        
        ########################################################################
        
        P_matrix = cvxopt.spmatrix(scaler.prof * self.price_model.k, 
                            range(len(self.price_model.k)),
                            range(len(self.price_model.k)))
        
        b_matrix = cvxopt.matrix((scaler.prof * 
                                  (self.price_model.b - 
                                   self.price_model.purch_price * 
                                   self.price_model.k)).reshape(1, -1))
        
        r = scaler.prof * (self.profit_constr + 
                           (self.price_model.b * 
                            self.price_model.purch_price).sum())
        
        profit_constr = p.T * P_matrix * p + b_matrix * p >= r 
        prob.add_constraint(profit_constr)
        
        ########################################################################
    
        try:
            logger.info('Running solver...')
            
            sol = prob.solve(verbose = cfg.debug_mode, 
                         tol = 1e-3, 
                         abstol = 1e-3, 
                         reltol = 1e-2,
                         feastol = 1e-6,
                         solver = 'cvxopt')
        
            logger.info('Solver done.')
            msg = sol['status']
            
            if msg == 'optimal' or msg == 'dual infeasible':
                status = opst.OPTIMAL
                prices = np.array(p.value).reshape(-1)
                
            elif msg == 'primal infeasible':
                prices = self.orig_prices
                status = opst.INCONSIST_CONSTR
                
            elif msg == 'unknown':
                if self.check_constraints():
                    status = opst.SUB_OPTIMAL
                else:
                    status = opst.INCONSIST_CONSTR    
                prices = np.array(p.value).reshape(-1)
                
            else:
                raise ValueError('Unknown status')
            
        except Exception as e:
            if str(e) == 'p is not valued':
                status = opst.INCONSIST_CONSTR
                prices = self.orig_prices
            else:
                raise
        
        return prices, status, msg
        
