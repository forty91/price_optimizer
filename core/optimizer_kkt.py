import cvxopt
import numpy as np
from core.errors import ErrorOptimizer
import core.optimizer_status as opst
import core.utils as utils
import core.core_config as cfg

import logging

logger = logging.getLogger('core_processing')


class OptimizerKkt(object):
    """ Wrapper for Kkt optimizer. """
    
    def __init__(self):
        """ No input parameters. """
        
        super(OptimizerKkt, self).__init__()
    
    def check_constraints(self, x, G, h, A, b):
        """ Checks whether constraints are satisfied with current variables. """
         
        tol_x = 0.1
        ineq_constr = ((np.matmul(G, x).reshape(-1, 1) - np.array(h)) 
                       >= tol_x).sum()
                        
        if ineq_constr != 0:
            return False

        eq_constr = np.dot(A, x)[0]
        b0 = b[0]
        rel_tol = 0.001
        abs_tol = 0.1
        if not utils.check_tol_eq(eq_constr, b0, rel_tol, abs_tol):
            return False
         
        return True
    
    def optimize(self, P, q, G, h, A, b):
        """ Runs optimization procedure. """
        
        msg = str()
             
        try:
            logger.info('Running solver...')
            
            sol = cvxopt.solvers.qp(P, q, G, h, A, b,
                                    options={'show_progress': cfg.debug_mode,
                                             'abstol': 1e-5,
                                             'reltol': 1e-4,
                                             'feastol': 1e-5})
            
            logger.info('Solver done.')
            
        except ValueError as e:
            msg = str(e)
            if msg == 'domain error':
                raise ErrorOptimizer('Invalid constraints.')
            else:
                raise ErrorOptimizer('Numerical error.')
        
        variables = np.array(sol['x']).reshape(-1)
        
        msg = sol['status']
        if msg == 'optimal' or msg == 'dual infeasible':
            status = opst.OPTIMAL
            
        elif msg == 'unknown':
            if self.check_constraints(variables, G, h, A, b):
                status = opst.SUB_OPTIMAL
            else:
                status = opst.INCONSIST_CONSTR
                
        elif msg == 'primal infeasible':
            status = opst.INCONSIST_CONSTR
            
        else:
            raise ValueError('Unknown status')
             
        return variables, status, msg
    
