from core.profit_optimizer_grad import ProfitOptimizerGrad
from core.profit_optimizer_kkt import ProfitOptimizerKkt
from core.demand_optimizer_kkt import DemandOptimizerKkt
from core.demand_optimizer_grad import DemandOptimizerGrad
#from experimental.optimizer_mmfd import OptimizerProfitMmfd


def create_optimizer(unpacked_data, 
                     task_size, 
                     demand_profit_constr,
                     task_str,
                     optimizer_str):
    """ 
    A factory for creating the required optimizer for the defined task. 
    """
    
    if task_str == 'opt_profit':
        if optimizer_str == 'kkt':
            optimizer = ProfitOptimizerKkt(*unpacked_data, 
                                           task_size=task_size,
                                           demand_constr=demand_profit_constr)
        elif optimizer_str == 'grad':
            optimizer = ProfitOptimizerGrad(*unpacked_data,
                                            task_size=task_size,
                                            demand_constr=demand_profit_constr)
        else:
            raise ValueError('Wrong optimizer "{}".'.format(optimizer_str))
        
    elif task_str == 'opt_demand':
        if optimizer_str == 'kkt':
            optimizer = DemandOptimizerKkt(*unpacked_data,
                                           task_size=task_size,
                                           profit_constr=demand_profit_constr)
        elif optimizer_str == 'grad':
            optimizer = DemandOptimizerGrad(*unpacked_data,
                                            task_size=task_size,
                                            profit_constr=demand_profit_constr)
        else:
            raise ValueError('Wrong optimizer "{}".'.format(optimizer_str))
        
    else:
        raise ValueError('Wrong task "{}".'.format(task_str))
    
    return optimizer

