""" Utils for checking of tolerance satisfaction.  """


def rel_tol_eq(X, Y):
    return float(abs(X - Y)) / min(abs(X), abs(Y))


def check_tol_eq(X, Y, rel_tol, abs_tol):
    m = min(abs(X), abs(Y))
    if m < abs_tol:
        return abs(X - Y) < abs_tol
    else:
        return rel_tol_eq(X, Y) < rel_tol


def rel_tol_less(X, Y):
    return float(X - Y) / min(abs(X), abs(Y))


def check_tol_less(X, Y, rel_tol, abs_tol):
    m = min(abs(X), abs(Y))
    if m < abs_tol:
        return (X - Y) < abs_tol
    else:
        return rel_tol_less(X, Y) < rel_tol

