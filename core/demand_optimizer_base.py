import utils


class DemandOptimizerBase(object):
    """ 
    Base class for maximization of demand with profit lower bound. 
    """
    
    def __init__(self,  
                 orig_prices,
                 min_prices, 
                 max_prices,
                 price_model, 
                 task_size, 
                 profit_constr):
        super(DemandOptimizerBase, self).__init__()
        
        self.task_size = task_size
        
        self.price_model = price_model
        
        self.min_prices = min_prices
        self.max_prices = max_prices
        self.orig_prices = orig_prices
        self.profit_constr = profit_constr
        
    def check_constraints(self, prices):
        """ 
        Checks whether constraints are satisfied with current variables. 
        """
        
        tol_min_max_price = 0.1
        tol_eq_constr = 0.001
        tol_abs = 0.1
        
        check = (prices - self.max_prices > tol_min_max_price).sum()
        if check > 0:
            return False
        
        check = (prices - self.min_prices < -tol_min_max_price).sum()
        if check > 0:
            return False
        
        if not utils.check_tol_less(self.profit_constr, 
                                    self.price_model.total_profit(prices), 
                                    tol_eq_constr, 
                                    tol_abs):
            return False
        
        return True
    