"""
Provides a class that finds prices for maximum profit with constraints using
kkt solver.
"""

from core.profit_optimizer_base import ProfitOptimizerBase
from core.optimizer_kkt import OptimizerKkt
import numpy as np

import cvxopt


class Scaler(object):
    """ 
    Scaler makes the task computationally feasible.
    Scaling as suggested in https://groups.google.com/forum/#!topic/cvxopt/MeEq5Q1Har0
             
    - Each row of A[i] and b[i] are renormalized by sum(abs(A[i]))
    - Each row of G[i] and h[i] are renormalized by sum(abs(G[i]))
    - P and q are renormalized with the spectral norm of P. 
    """
    
    def __init__(self, price_model):
        """ Initialize scaler: price model from optimizer. """

        self.price_model = price_model
    
    @property
    def target(self):
        """ Scaler for target function. P and q matrices. """
        
        #scaler = 0.01 # DEBUG:
        scaler = abs(1. / max(abs(self.price_model.k)))
        return scaler
    
    @property
    def ineq(self):
        """ Scaler for inequality constraints. G and h matrices. """
        
        scaler = 1.; # Looks OKish.
        return scaler;
    
    @property
    def eq(self):
        """ Scaler for equality constraints. A and b matrices. """
        
        scaler = 1. / abs(self.price_model.k).sum()
        return scaler


class ProfitOptimizerKkt(ProfitOptimizerBase):
    """ Optimizer for maximum profit by means Karush Kuhn Tucker conditions. """
     
    def __init__(self, *args, **kwargs):
        """
        Parameters for initializations:
            orig_prices,
            min_prices, 
            max_prices,
            price_model, 
            task_size, 
            demand_constr
        """
        
        super(ProfitOptimizerKkt, self).__init__(*args, **kwargs)
    
    def optimize(self):
        """ Starts optimization procedure. """
        
        scaler = Scaler(self.price_model)
        
        P = cvxopt.spmatrix([scaler.target * -2. * k for k in self.price_model.k],
                            range(len(self.price_model.k)),
                            range(len(self.price_model.k)))

        q = cvxopt.matrix(scaler.target * -1. * (self.price_model.b -
                                                 self.price_model.purch_price *
                                                 self.price_model.k))

        G = cvxopt.matrix(scaler.ineq * 
                          np.concatenate((1. * np.eye(self.task_size), 
                                          -1. * np.eye(self.task_size)), 
                                         axis=0))

        h = cvxopt.matrix(scaler.ineq *
                          np.concatenate((1. * self.max_prices,
                                          -1. * self.min_prices)))
        
        A = self.price_model.k.reshape((1, -1))
        A = cvxopt.matrix(scaler.eq * A)

        b = cvxopt.matrix(scaler.eq * 
                          (self.demand_constr - self.price_model.b.sum()))
        
        return OptimizerKkt().optimize(P, q, G, h, A, b) 
