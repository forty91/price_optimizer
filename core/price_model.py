class PriceModel(object):
    """ Stores information about linear price model (p - price): kp + b . """
    
    def __init__(self, k=None, b=None, purch_price=None):
        self.k = k
        self.b = b
        
        self.purch_price = purch_price
        
    def demand(self, price, k, b):
        demand_value = k * price + b
        return demand_value
    
    def profit(self, purch_price, price, demand):
        return (price - purch_price) * demand
    
    def total_profit(self, prices):
        return self.profit(self.purch_price, 
                           prices, 
                           self.demand(prices, self.k, self.b)).sum(axis = 0)
    
    def total_demand(self, prices):
        return self.demand(prices, self.k, self.b).sum(axis = 0)
