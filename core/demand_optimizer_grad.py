"""
Provides a class that finds prices for maximum demand with constraints using 
gradient-based solver.
"""

from core.demand_optimizer_base import DemandOptimizerBase
import scipy.optimize
import core.optimizer_status as opst
import math
import core.core_config as cfg


def tune_tolerance(task_size):
    """ 
    Tunes tolerance for different task sizes. It allows more precision for 
    simple tasks. 
    """
    
    return max(1e-5, 
               min(1., 
                   0.1 ** math.floor(5000. / 
                                     ((task_size + 50) * 
                                      math.sqrt(1. * task_size / 50)))))


class DemandOptimizerGrad(DemandOptimizerBase):
    """ 
    Maximizes demand using gradient-based method. 
    Slow on big tasks (number of goods > 1500).
    """
    
    def __init__(self, *args, **kwargs):
        """
        Parameters for initializations:
            orig_prices,
            min_prices, 
            max_prices,
            price_model, 
            task_size, 
            profit_constr
        """
        
        super(DemandOptimizerGrad, self).__init__(*args, **kwargs)
        
    def optimize(self):
        """ Starts optimization procedure. """
        
        res = scipy.optimize.minimize(fun = self._opt_func, 
                                      jac = self._opt_func_jac,
                                      x0 = self.orig_prices, 
                                      method = 'SLSQP', 
                                      bounds = zip(self.min_prices, 
                                                   self.max_prices),
                                      constraints = 
                                      {'type' : 'ineq', 
                                       'fun' : self._profit_constr_func, 
                                       'jac' : self._profit_constr_func_jac},
                                      options = {'disp' : cfg.debug_mode},
                                      tol = tune_tolerance(self.task_size))
        
        if res.success:
            if self.check_constraints(res.x):
                result = opst.OPTIMAL
            else:
                result = opst.INCONSIST_CONSTR
        else:
            result = opst.INCONSIST_CONSTR
            
        return res.x, result, res.message
    
    def _opt_func(self, prices):
        return -1. * self.price_model.total_demand(prices)
    
    def _opt_func_part_prime(self, price, k):
        return -1. * k
    
    def _opt_func_jac(self, prices):
        return self._opt_func_part_prime(prices, self.price_model.k)
    
    def _profit_constr_func(self, prices):
        return self.price_model.total_profit(prices) - self.profit_constr
    
    def _profit_constr_func_part_prime(self, price, k, b, purch_price):
        return 2. * k * price + b - purch_price * k
    
    def _profit_constr_func_jac(self, prices):
        return self._profit_constr_func_part_prime(prices, 
                                                   self.price_model.k, 
                                                   self.price_model.b, 
                                                   self.price_model.purch_price)
        
