""" Specific exceptions used in the package. """

class ErrorOptimizer(Exception):
    pass

class ErrorInputDataFormat(Exception):
    pass

class ErrorInvalidPriceModel(Exception):
    pass

