from locale import setlocale, LC_NUMERIC, atof, atoi


#set russian numeric representation of floats ("," for precision delimiter)
setlocale(LC_NUMERIC, ('RU', 'UTF8'))
