# coding: utf-8

import numpy

from flask import Flask
from flask_restful import Resource, Api
from werkzeug.contrib.fixers import ProxyFix
from flask_cors import CORS

from bl import PriceOptimizer
from models import Session, Product
from serializers import product_serializer


app = Flask(__name__)
api = Api(app)
CORS(app, origins='*', methods=['GET', 'OPTIONS'])


class OptimizeDemandByProfitCount(Resource):
    def get(self, points_count):
        # segments = numpy.linspace(0, 1, int(points_count), endpoint=False)
        segments = numpy.linspace(0, 1, int(points_count))

        result = {
            'result': PriceOptimizer().optimize_demand_by_profit_segments(segments[1: -1])
        }

        return result


class OptimizeDemandByProfitStep(Resource):
    def get(self, step):

        result = {
            'result': PriceOptimizer().optimize_demand_by_profit_steps(int(step))
        }

        return result


class OptimizeProfitByDemandStep(Resource):
    def get(self, step):

        result = {
            'result': PriceOptimizer().optimize_profit_by_demand_steps(int(step))
        }

        return result


class GetProducts(Resource):
    def get(self):
        session = Session()
        products = session.query(Product).all()
        session.close()

        result = {
            'products': [product_serializer(product) for product in products]
        }

        return result


class GetProductByMaterialId(Resource):
    def get(self, matterial_id):
        session = Session()
        product = session.query(Product).filter_by(material=matterial_id).first()
        session.close()

        return product_serializer(product)


api.add_resource(OptimizeDemandByProfitCount, '/optimize/<string:points_count>', endpoint='optimize')
# api.add_resource(OptimizeDemandByProfitStep, '/optimize/step/<string:step>')
api.add_resource(OptimizeProfitByDemandStep, '/optimize/step/<string:step>')
api.add_resource(GetProducts, '/')
api.add_resource(GetProductByMaterialId, '/<int:matterial_id>')


app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == '__main__':
    # app.run(debug=True, port=8000)
    app.run()
