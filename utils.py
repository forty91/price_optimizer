# coding: utf-8

import os
import csv
from itertools import islice

from settings import atof, atoi
from models import Product, Session


def get_base_dir():
    return os.path.dirname(os.path.abspath(__file__))


def fill_products_db(csv_file_fullname):
    session = Session()

    try:
        session.query(Product).delete()

        with open(csv_file_fullname) as csv_file:
            csv_data = csv.reader(csv_file, delimiter=';', quoting=csv.QUOTE_NONE)

            # for row in islice(csv_data, 1, None):
            for row in csv_data:
                record = Product(
                    material=atoi(row[1]),
                    name=row[2],
                    orig_price=atof(row[3]),
                    purch_price=atof(row[4]),
                    k=atof(row[5]),
                    b=atof(row[6]),
                    min_price=atof(row[7]),
                    max_price=atof(row[8]),
                    avg_qnt=atof(row[9]),
                    opt_price=atof(row[10]),
                )
                session.add(record)

            session.commit()

    except (OSError, IOError) as e:
        raise Exception(u'В настройках указан неправильный путь до исходного .csv файла!')
    except Exception as e:
        session.rollback()
        raise Exception(u'Неизвестная ошибка при загрузке .csv файла в базу! ')
    finally:
        session.close()


def save_opt_prices(prices):
    session = Session()

    try:
        products = session.query(Product).all()

        if len(products) != len(prices):
            raise Exception(u'Количество оптимальных цен для обновления не равно количеству товаров в базе')

        for product, price in zip(products, prices):
            product.opt_price = price

        session.commit()

    except Exception as e:
        session.rollback()
        raise Exception(u'Неизвестная ошибка при загрузке .csv файла в базу! ' +
                        u'Внимание, первая строка должна содержать только названия полей!')
    finally:
        session.close()


# Пример загрузки файла в базу
# fill_products_db(u'/home/forty/projects/price_optimizer/sap_forum.csv')


