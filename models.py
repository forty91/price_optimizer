# coding: utf-8

from sqlalchemy import create_engine
from sqlalchemy import Column, String, Integer, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine("sqlite:///price_optimizer.db", echo=True)
Base = declarative_base()

Session = sessionmaker(bind=engine)


class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, autoincrement=True, primary_key=True)
    material = Column(Integer)
    name = Column(String(length=100))
    orig_price = Column(Float(precision=2))
    purch_price = Column(Float(precision=2))
    k = Column(Float(precision=2))
    b = Column(Float(precision=2))
    min_price = Column(Float(precision=2))
    max_price = Column(Float(precision=2))
    avg_qnt = Column(Float(precision=2))
    opt_price = Column(Float(precision=2))


# В случае отсутствия важной таблицы, считаем что файл с БД отсутствует
# и потому создаем базу из моделей
if not engine.dialect.has_table(engine, 'products'):
    Base.metadata.create_all(engine)
