def product_serializer(product):
    """
    :param product: models.Product
    :rtype: dict
    """
    serialized_data = {
        'id': product.id,
        'material_id': product.material,
        'name': product.name,
        'orig_price': product.orig_price,
        'purch_price': product.purch_price,
        'k': product.k,
        'b': product.b,
        'min_price': product.min_price,
        'max_price': product.max_price,
        'avg_qnt': product.avg_qnt,
        'opt_price': product.opt_price
    }

    return serialized_data
