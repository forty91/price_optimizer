# coding: utf-8

from core import data_format
from core.factory_optimizer import create_optimizer
from models import Product, Session
from utils import save_opt_prices


class PriceOptimizer(object):
    def __init__(self):
        session = Session()
        self.products = session.query(Product).all()
        session.close()
        self.unpacked_data = data_format.unpack_data(self.products)
        self.price_model = self.unpacked_data[3]

    def find_min_profit(self):
        min_profit = 0.0
        for product in self.products:
            # min_profit += product.orig_price
            min_profit += product.opt_price

        return min_profit

    def find_max_profit(self):
        orig_demand_sum = 0.0
        for product in self.products:
            orig_demand_sum += product.avg_qnt

        optimizer = create_optimizer(
            unpacked_data=self.unpacked_data,
            task_size=len(self.products),
            demand_profit_constr=orig_demand_sum,
            task_str='opt_profit',
            optimizer_str='kkt'
        )
        opt_prices, result, msg = optimizer.optimize()
        max_profit = self.price_model.total_profit(opt_prices)

        return max_profit

    def find_min_demand(self):
        min_profit = 0.0
        for product in self.products:
            min_profit += product.avg_qnt

        return min_profit

    def find_max_demand(self):
        orig_profit_sum = 0.0
        for product in self.products:
            orig_profit_sum += product.opt_price

        optimizer = create_optimizer(
            unpacked_data=self.unpacked_data,
            task_size=len(self.products),
            demand_profit_constr=orig_profit_sum,
            task_str='opt_demand',
            optimizer_str='kkt'
        )
        opt_prices, result, msg = optimizer.optimize()
        max_demand = self.price_model.total_demand(opt_prices)

        return max_demand

    def optimize_demand_by_profit(self, profit_constr):
        optimizer = create_optimizer(
            unpacked_data=self.unpacked_data,
            task_size=len(self.products),
            demand_profit_constr=profit_constr,
            task_str='opt_demand',
            optimizer_str='kkt'
        )
        opt_prices, result, msg = optimizer.optimize()
        opt_demand_sum = self.price_model.total_demand(opt_prices)

        # save_opt_prices(opt_prices)

        result = {
            'profit': profit_constr,
            'demand': opt_demand_sum,
            'goods': [{'material_id': product.material, 'price': price} for (product, price) in zip(self.products, opt_prices)]
        }

        return result

    def optimize_profit_by_demand(self, demand_constr):
        optimizer = create_optimizer(
            unpacked_data=self.unpacked_data,
            task_size=len(self.products),
            demand_profit_constr=demand_constr,
            task_str='opt_profit',
            optimizer_str='kkt'
        )
        opt_prices, result, msg = optimizer.optimize()
        opt_profit_sum = self.price_model.total_profit(opt_prices)

        # save_opt_prices(opt_prices)

        result = {
            'profit': opt_profit_sum,
            'demand': demand_constr,
            'goods': [{'material_id': product.material, 'price': price} for (product, price) in zip(self.products, opt_prices)]
        }

        return result

    def optimize_demand_by_profit_segment(self, segment):
        """
        Оптимизация спроса(demand) по прибыли(profit)
        :param segment: доля возможной прибыли между min_profit max_profit
        """

        min_profit = self.find_min_profit()
        max_profit = self.find_max_profit()

        if segment == 1.0:
            profit_constr = max_profit
        elif segment == 0.0:
            profit_constr = min_profit
        else:
            profit_constr = min_profit + (max_profit - min_profit) * segment

        return self.optimize_demand_by_profit(profit_constr)

    def optimize_demand_by_profit_segments(self, segments):
            return [self.optimize_demand_by_profit_segment(round(segment, 3)) for segment in segments]

    def optimize_demand_by_profit_steps(self, step_size):
        result = []
        min_profit = self.find_min_profit()
        max_profit = self.find_max_profit()

        profit_constr = min_profit
        while profit_constr < max_profit:
            result.append(self.optimize_demand_by_profit(profit_constr))
            profit_constr += min_profit * step_size / 100.0

        return result

    def optimize_profit_by_demand_steps(self, step_size):
        result = []
        min_demand = self.find_min_demand()
        max_demand = self.find_max_demand()

        demand_constr = min_demand
        while demand_constr < max_demand:
            result.append(self.optimize_profit_by_demand(demand_constr))
            demand_constr += min_demand * step_size / 100.0

        return result
