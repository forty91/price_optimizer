# Развертывание price_optimizer на SAP Cloud Platform

##### 1) Устанавливаем клиент на Ubuntu
```sh
wget -q -O - https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | sudo apt-key add -
echo "deb http://packages.cloudfoundry.org/debian stable main" | sudo tee /etc/apt/sources.list.d/cloudfoundry-cli.list
sudo apt-get update
sudo apt-get install cf-cli
```

##### 2) Подключаемся к имеющемуся у нас аккаунту
```sh
cf api https://api.cf.us10.hana.ondemand.com
cf login
```

##### 3) Загружаем проект
Переходим в корень проектаи запускаем скрипт установки
```sh
cf push -c "bash ./cf_init.sh"
```

##### Если что то пошло не так
Смотрим логи
```sh
cf logs price_optimizer --recent
```
Смотрим запущенные в данный момент сервисы
```sh
cf services
```
Удаляем сервисы, которые нам не нужны или запустилис но не как нужно
```sh
cf delete optix
```
